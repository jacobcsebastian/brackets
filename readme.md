# Balanced Brackets

Given a string consisting entirely of the characters ()[]{}, determine if it is "balanced".

That is, every opening bracket must have a closing bracket of the same type following it, and the string in between the pair must also be balanced.

### Running the project

In the terminal, execute the command
```
npm run balanced "{}{}{}"
```

### Running the tests

In the terminal, execute the command

```
npm run test
```

### Outcomes

if balanced, it would output **balanced** and if not, it would display **not balanced**.