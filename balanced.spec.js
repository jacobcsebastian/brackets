'use strict'

const isBalanced = require('./balanced').isBalanced;
const expect = require('chai').expect;
const assert = require('chai').assert;
const _ = require('lodash');

describe('User module', () => {
  describe('"up"', () => {
    it('should export a function', () => {
      expect(isBalanced).to.be.a('function');
    });

    it('should throw exceptions if characters are not brackets', () => {
      expect(() => isBalanced('no_brackets')).to.throw('Should only have brackets');
    });

    it('should not throw exceptions if characters are brackets', () => {
      expect(() => isBalanced('{}{}{{{{{]]])()')).not.to.throw('Should only have brackets');
    });
    describe('Balanced checks', () => {
      it('should return balanced when brackets are balanced', () => {
        assert(isBalanced('[]') === 'Balanced');
        assert(isBalanced('()[]{}(([])){[()][]}') === 'Balanced');
        assert(isBalanced('()[{{}}]([{}]{})') === 'Balanced');
        assert(isBalanced('[{}]') === 'Balanced');
        assert(isBalanced('[{()}]') === 'Balanced');
        assert(isBalanced('[{()[][](){{[()]}({}([]))}}]') === 'Balanced');
        assert(isBalanced('({()})(){}[]') === 'Balanced');
        assert(isBalanced('[{}{}{}{{{{{{(()){{{{{{{{[[[[[[[[[[[]]]]]]]]]][[[[[[]]]]]]]}}}}}}}}}}}}}}]') === 'Balanced');
      });
    });
    describe('Not balanced Checks', () => {
      it('should return balanced when brackets are not balanced', () => {
        assert(isBalanced('[}]') === 'Not balanced');
        assert(isBalanced('[(])') === 'Not balanced');
        assert(isBalanced('[{)}]') === 'Not balanced');
        assert(isBalanced('())[]{}') === 'Not balanced');
        assert(isBalanced('[{())}]') === 'Not balanced');
        assert(isBalanced('[{()[])[](){{[()]}({}([]))}}]') === 'Not balanced');
        assert(isBalanced('({()}))(){}[]') === 'Not balanced');
        assert(isBalanced('[{}{}{}){{{{{{(()){{{{{{{{[[[[[[[[[[[]]]]]]]]]][[[[[[]]]]]]]}}}}}}}}}}}}}}]') === 'Not balanced');
      });
    });


  })
})