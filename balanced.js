
const _ = require('lodash');

const openingBrackets = ['{', '[', '('];
const closingBrackets = ['}', ']', ')'];
const bracketRef = {
  '{': '}',
  '[': ']',
  '(': ')'
};

module.exports = {
  isBalanced: function (data) {
    const stack = new Array();
    const brackets = data.split('');
    if(_.difference(brackets, openingBrackets.concat(closingBrackets)).length > 0) {
      throw new Error('Should only have brackets');
    }

    brackets.forEach(item => {
      if (stack.length === 0) {
        stack.push(item);
        return;
      }
      const isClosingBracket = closingBrackets.includes(item);
      const isOpeningBracket = openingBrackets.includes(item);
  
      if (isClosingBracket) {
        const lastElement = _.last(stack);
        if (bracketRef[lastElement] === item) {
          stack.pop();
        } else {
          stack.push(item);
        }
      }
      if (isOpeningBracket) {
        stack.push(item);
      }
    });
    if (stack.length === 0) {
      return 'Balanced';
    } else {
      return 'Not balanced';
    }

  }
}
require('make-runnable');
